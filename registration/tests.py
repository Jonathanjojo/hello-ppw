from django.apps import apps
from django.test import TestCase
from .apps import RegistrationConfig
from .models import User
from .views import (
    index,
    user_check,
    user_create
)
from django.urls import resolve
import json
import requests

# Create your tests here.
class RegistrationAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(RegistrationConfig.name, 'registration')
        self.assertEqual(apps.get_app_config('registration').name, 'registration')

class RegisterPageTest(TestCase):
    def test_registration_page_accessible(self):
        response = self.client.get('/registration/')
        self.assertEqual(response.status_code, 200)

    def test_registration_page_using_index_function(self):
        found = resolve('/registration/')
        self.assertEqual(found.func, index)
    
    def test_registration_page_uses_correct_template(self):
        response = self.client.get('/registration/')
        self.assertTemplateUsed(response, 'registration/index.html')

class UserModelTest(TestCase):
    def test_default_attribute(self):
        user = User()
        self.assertEqual(user.username, '')
        self.assertEqual(user.email, '')
        self.assertEqual(user.password, '')

    def test_user_string_representation(self):
        user = User(username="username", email="email@email.com", password="password")
        self.assertEqual(str(user), "username - email@email.com")

    def test_user_verbose_name_plural(self):
        self.assertEqual(str(User._meta.verbose_name_plural), "users")

class UserCheckingTest(TestCase):
    def test_post_ajax_request_to_check_user(self):
        response = self.client.post(
            '/registration/user_check/', 
            json.dumps({
                'username' : 'username', 
                'email' : 'email@email.com',
                'password' : 'password',
                'confirmation_password' : 'password'
            }),
            content_type="application/json", 
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        data = response.json()
        self.assertTrue(data['valid'])
        self.assertTrue(data['pswd_confirmed'])
        self.assertFalse(data['existing_user'])

    def test_get_ajax_request_to_check_user(self):
        response = self.client.get('/registration/user_check/')
        self.assertEqual(response.status_code, 405)
    
class UserCreationTest(TestCase):
    def test_post_ajax_request_to_create_user(self):
        response = self.client.post(
            '/registration/user_create/', 
            json.dumps({
                'username' : 'username', 
                'email' : 'email@email.com',
                'password' : 'password'
            }),
            content_type="application/json", 
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.status_code, 201)

    def test_get_ajax_request_to_create_user(self):
        response = self.client.get('/registration/user_create/')
        self.assertEqual(response.status_code, 405)