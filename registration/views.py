from django.http import JsonResponse
from django.shortcuts import render, HttpResponse
from .forms import UserInfoForm
from .models import User
import json

# Create your views here.
def index(request):
    form = UserInfoForm()
    users = User.objects.all()
    response = {
        "form"  : form
    }
    return render(request, 'registration/index.html', response)

def user_list(request):
    if request.is_ajax():
        users = User.objects.values("id", "username")
        return JsonResponse({
            "users" : list(users)
        })
    return HttpResponse("Method not allowed", status = 405)

def user_check(request):
    if request.method == "POST" and request.is_ajax():
        received_json_data = json.loads(request.body, encoding='UTF-8')
        existing_user = User.objects.filter(email=received_json_data['email']).first()
        form = UserInfoForm({
            'username' : received_json_data['username'],
            'email' : received_json_data['email'],
            'password' : received_json_data['password'],
            'confirmation_password' : received_json_data['confirmation_password']
        })
        password_confirmed = received_json_data['password'] == received_json_data['confirmation_password']
        return JsonResponse({
            "valid"         : form.is_valid(),
            "pswd_confirmed": password_confirmed,
            "existing_user" : existing_user != None
        })
    return HttpResponse("Method not allowed", status = 405)

def user_create(request):
    if request.method == 'POST' and request.is_ajax():
        received_json_data = json.loads(request.body, encoding='UTF-8')        
        User.objects.create(
            username = received_json_data['username'],
            email = received_json_data['email'],
            password = received_json_data['password']
        )
        return HttpResponse("New user succesfully added", status=201)
    else:
        return HttpResponse("Method not allowed", status = 405)

def user_unsub(request):
    if request.method == "POST" and request.is_ajax():
        received_json_data = json.loads(request.body, encoding='UTF-8')    
        user = User.objects.get(id=received_json_data['id'])
        password_confirmed = user.password == received_json_data['password']
        if password_confirmed:
            user.delete()
        return JsonResponse({
            "pswd_confirmed": password_confirmed,
        })
    return HttpResponse("Method not allowed", status = 405) 