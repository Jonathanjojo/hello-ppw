from django.urls import path
from . import views

app_name = "registration"

urlpatterns = [
    path('', views.index, name="index"),
    path('user_unsub/', views.user_unsub, name="user_unsub"),
    path('user_list/', views.user_list, name="user_list"),
    path('user_check/', views.user_check, name="user_check"),
    path('user_create/', views.user_create, name="user_create"),
]