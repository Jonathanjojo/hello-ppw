from . import models
from django import forms

class UserInfoForm(forms.Form):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
            "class"         : "fields",
            "required"      : True,
            "onInput"       : "validateUserInfo()",
            "placeholder"   : "Your Name"
    }), max_length=50)

    email = forms.EmailField(label="E-mail", widget=forms.EmailInput(attrs={
            "class"         : "fields",
            "required"      : True,
            "onInput"       : "validateUserInfo()",
            "placeholder"   : "Your Email"
    }), max_length=50)

    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={
            "class"         : "fields",
            "required"      : True,
            "onInput"       : "validateUserInfo()",
            "placeholder"   : "Your Password"
    }), max_length=50)

    confirmation_password = forms.CharField(label="Confirm Password", widget=forms.PasswordInput(attrs={
            "class"         : "fields",
            "required"      : True,
            "onInput"       : "validateUserInfo()",
            "placeholder"   : "Your Password Again"
    }), max_length=50)

    class Meta:
        model = models.User
