from django.apps import apps
from django.test import TestCase
from django.urls import resolve
from .apps import AjaxConfig
from .views import default_index, index, json_process
from .models import FavoriteBook
import json
import requests

# Create your tests here.

class BookAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(AjaxConfig.name, 'ajax')
        self.assertEqual(apps.get_app_config('ajax').name, 'ajax')

class BookPageTest(TestCase):
    def test_book_page_accessible(self):
        response = self.client.get('/ajax/quilting/')
        self.assertEqual(response.status_code, 200)

    def test_book_page_using_index_function(self):
        found = resolve('/ajax/')
        self.assertEqual(found.func, default_index)
        response = self.client.get('/ajax/')
        self.assertEqual(response.status_code, 302)
    
    def test_book_page_uses_correct_template(self):
        response = self.client.get('/ajax/quilting/')
        self.assertTemplateUsed(response, 'ajax/index.html')

    def test_rendered_table(self):
        response = self.client.get('/ajax/table/quilting/')
        self.assertEqual(response.status_code, 200)

    def test_json_returned(self):
        response = self.client.get('/ajax/api/quilting/')
        data = response.json()
        FavoriteBook.objects.create(title=data['items'][0]['title'], publisher=data['items'][0]['publisher'])
        response = self.client.get('/ajax/api/quilting/')
        data = response.json()
        self.assertEqual("items" in data, True)

class BookModelTest(TestCase):
    def test_book_string_representation(self):
        book = FavoriteBook(title="TITLE", publisher="PUBLISHER")
        self.assertEqual(str(book), "TITLE - PUBLISHER")

    def test_book_verbose_name_plural(self):
        self.assertEqual(str(FavoriteBook._meta.verbose_name_plural), "Favorite Books")

class AdditionRemovalTest(TestCase):
    def test_book_added_before(self):
        json_request = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
        original_data = json_request.json()
        book = FavoriteBook()
        book.title = original_data['items'][0]['volumeInfo']['title']
        book.publisher = original_data['items'][0]['volumeInfo']['publisher']
        book.save()
        response = self.client.get('/ajax/quilting/')
        self.assertContains(response, "Added")

    def test_get_request_to_process_json(self):
        response = self.client.get('/ajax/json_process/')
        self.assertEqual(response.status_code, 405)

    # Simulate ajax request (pass is_ajax conditional) and how to send request in json form
    # http://www.ericholscher.com/blog/2009/apr/16/testing-ajax-views-django/
    # https://stackoverflow.com/questions/8583290/sending-json-using-the-django-test-client

    def test_post_ajax_request_to_process_book_addition(self):
        response = self.client.post('/ajax/json_process/', json.dumps({'title':'TITLE', 'publisher':'PUBLISHER'}), content_type="application/json", HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 201)

    def test_post_ajax_request_to_process_book_removal(self):
        prev_added_book = FavoriteBook.objects.create(title="TITLE", publisher="PUBLISHER")
        response = self.client.post('/ajax/json_process/', json.dumps({'title':'TITLE', 'publisher':'PUBLISHER'}), content_type="application/json", HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 208)