from django.urls import path
from . import views

app_name = "ajax"

urlpatterns = [
    path('', views.default_index, name="default_index"),
    path('json_process/', views.json_process, name='json_process'),
    path('api/<query_search>/', views.api, name="api"),
    path('table/<query_search>/', views.render_table, name="table"),
    path('<query_search>/', views.index, name="index"),
]