from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, HttpResponse, redirect
from .models import FavoriteBook
import json
import requests
from django.http import JsonResponse

def default_index(request):
    return redirect('ajax:index', query_search="quilting")

@csrf_exempt
def api(request, query_search):
    json_request = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + query_search + "/")
    original_data = json_request.json()
    book_queries = {
        'items' : [],
        'added_books' : 0,
    }
    for book in original_data['items']:
        new_book = {
            'title'         : '',
            'publisher'     : '',
            'authors'       : []
        }
        if 'title' in book['volumeInfo']:
            new_book['title'] = book['volumeInfo']['title']
        if 'publisher' in book['volumeInfo']:
            new_book['publisher'] = book['volumeInfo']['publisher']
        if 'authors' in book['volumeInfo']:
            new_book['authors'] = [author for author in book['volumeInfo']['authors']]
        if FavoriteBook.objects.filter(title=new_book['title'], publisher=new_book['publisher']).first():
            new_book['added'] = True
            book_queries['added_books'] += 1
        else:
            new_book['added'] = False            
        book_queries['items'].append(new_book)
    return JsonResponse(book_queries)

def index(request, query_search):
    book_queries = requests.get("https://hello-joe.herokuapp.com/ajax/api/"+query_search+"/")
    book_queries = book_queries.json()
    return render(request, 'ajax/index.html', book_queries)

def render_table(request, query_search):
    book_queries = requests.get("https://hello-joe.herokuapp.com/ajax/api/"+query_search+"/")
    book_queries = book_queries.json()
    return render(request, 'ajax/table.html', book_queries)

@csrf_exempt
def json_process(request):
    if request.method == 'POST' and request.is_ajax():
        received_json_data = json.loads(request.body, encoding='UTF-8')
        book_title = received_json_data['title']
        book_publisher = received_json_data['publisher']
        book_entry = FavoriteBook.objects.filter(title=book_title, publisher=book_publisher).first()
        if book_entry == None:
            FavoriteBook.objects.create(
                title = received_json_data['title'], 
                publisher = received_json_data['publisher']
            )    
            return HttpResponse("Added to Favorite : {} - {}".format(book_title, book_publisher), status=201 )   
        else:
            book_entry.delete()
            return HttpResponse("Already Added Before", status=208)
    return HttpResponse("Method not Allowed", status=405)