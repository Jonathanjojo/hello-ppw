from django.contrib import admin
from .models import FavoriteBook

# Register your models here.
admin.site.register(FavoriteBook)
