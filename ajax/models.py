from django.db import models

# Create your models here.
class FavoriteBook(models.Model):
    title = models.CharField(max_length=100)
    publisher = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Favorite Books"
        
    def __str__(self):
        return "{} - {}".format(self.title, self.publisher)