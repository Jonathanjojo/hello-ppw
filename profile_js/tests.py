from django.test import TestCase
from django.urls import resolve
from .views import index

# Create your tests here.
class HomePageTests(TestCase):
    def test_profile_page_accessible(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_profile_page_uses_correct_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'profile_js/profile.html')
