from django.apps import AppConfig


class ProfileJsConfig(AppConfig):
    name = 'profile_js'
