from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import unittest
import time

# class VisitorTest(unittest.TestCase):
#     def setUp(self):  
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')

#         self.browser = webdriver.Chrome(chrome_options=options)
#         self.page_url = 'http://localhost:8000/'

#     def tearDown(self):  
#         self.browser.quit()

    # def test_profile_page_contains_correct_contents(self):
    #     self.browser.get(self.page_url)
    #     time.sleep(10)

    #     # assert title
    #     self.assertIn("Hello, I'm Jojo", self.browser.find_element_by_id('page-title').text) 

    #     # assert to-anchor button
    #     self.assertEqual(self.browser.find_element_by_id('to-info').get_attribute('href'), self.page_url+'#info')
    #     # assert toggle-theme button
    #     toggle_button = self.browser.find_element_by_id("toggle-style-sheet")
    #     self.assertEqual(self.browser.find_element_by_tag_name('body').value_of_css_property('background-color'), 'rgba(0, 0, 17, 1)')
    #     self.assertEqual(self.browser.find_elements_by_css_selector('.primary-text')[0].value_of_css_property('color'), 'rgba(0, 219, 160, 1)')
    #     self.assertEqual(self.browser.find_elements_by_css_selector('.secondary-text')[0].value_of_css_property('color'), 'rgba(237, 237, 237, 1)')
        
    #     toggle_button.click()
    #     time.sleep(3)

    #     self.assertEqual(self.browser.find_element_by_tag_name('body').value_of_css_property('background-color'), 'rgba(255, 255, 255, 1)')
    #     self.assertEqual(self.browser.find_elements_by_css_selector('.primary-text')[0].value_of_css_property('color'), 'rgba(255, 92, 63, 1)')
    #     self.assertEqual(self.browser.find_elements_by_css_selector('.secondary-text')[0].value_of_css_property('color'), 'rgba(0, 0, 0, 1)')

    #     toggle_button.click()
    #     time.sleep(3)

    #     self.assertEqual(self.browser.find_element_by_tag_name('body').value_of_css_property('background-color'), 'rgba(0, 0, 17, 1)')
    #     self.assertEqual(self.browser.find_elements_by_css_selector('.primary-text')[0].value_of_css_property('color'), 'rgba(0, 219, 160, 1)')
    #     self.assertEqual(self.browser.find_elements_by_css_selector('.secondary-text')[0].value_of_css_property('color'), 'rgba(237, 237, 237, 1)')

    #     # assert accordions
    #     self.assertEqual(len(self.browser.find_elements_by_css_selector('#info > .row > .col-md-4 > .accordion')), 3)
    #     # structure would be:
    #     # div #info (1 page)
    #     #     div row
    #     #         div col (3 columns)
    #     #             accordion   
    #     #                 accordion title
    #     #                 accordion content
    #     # accordion_titles = self.browser.find_elements_by_css_selector("#info > .row > .col-md-4 > .accordion > .accordion_title")
    #     # for index, accordion_title in enumerate(accordion_titles):
    #     #     accordion_title.click()
    #     #     content = self.browser.find_elements_by_xpath('//*[@id="ui-id-' + str(2*(index+1)) + '"]')
    #     #     self.assertTrue(content[0].is_displayed())
