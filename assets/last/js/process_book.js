function initTable(){
    let csrf_token_entry = $('input[name="csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        url: 'books_api/quilting/',
        type: 'GET',
        dataType: 'json',
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(result) {
            updateTable(result);
        }
    });
}

function searchBook(){
    let searchQuery = document.getElementById("searchInput").value;
    let csrf_token_entry = $('input[name="csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        url: 'books_api/' + searchQuery + '/',
        type: 'GET',
        dataType: 'json',
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(result) {
            updateTable(result);
        }
    });
}

function updateTable(result){
    let table_holder = document.getElementById('book-table');
    let table_head = document.getElementById('table-head')
    table_holder.innerHTML = "";
    table_holder.appendChild(table_head);
    result.items.forEach(function(item, i) {
        let tr = document.createElement("tr");
        let no = document.createElement("td");
        no.innerText = i + 1;
        no.classList.add("bold");
        tr.appendChild(no);
        let title = document.createElement("td");
        title.innerText = item.title;
        tr.appendChild(title);
        let author_list = document.createElement("ul");
        author_list.classList.add("author-list");
        item.authors.forEach(function(author_name, j) {
            let author = document.createElement("li");
            author.innerText = author_name;
            author_list.appendChild(author);
        });
        tr.appendChild(author_list);
        let publisher = document.createElement("td");
        publisher.innerText = item.publisher;
        tr.appendChild(publisher);
        let button_col = document.createElement("td");
        button_col.classList.add("fav-col");
        let form = document.createElement("form");
        let link = document.createElement("a");
        link.id = "book-" + (i+1);
        link.value = "notAdded";
        link.onclick = function() { 
            toggleBookAddition("book-" + (i+1)); 
        };
        link.classList.add("book-link");
        let image = document.createElement("img");
        image.id = "book-" + (i+1) + "-image";
        image.classList.add("fav-icon");
        image.src = grey_star_src;
        link.appendChild(image);
        form.appendChild(link);
        button_col.appendChild(form);
        tr.appendChild(button_col);
        table_holder.append(tr);
    });
}

function toggleBookAddition(bookID){
    var pointedButton = document.getElementById(bookID);
    var bookCounter = document.getElementById("book-counter");
    var buttonImage = document.getElementById(bookID+"-image");
    if(pointedButton.getAttribute('value') != "Added"){
        addBook(pointedButton, buttonImage, bookCounter);
    } else {
        removeBook(pointedButton, buttonImage, bookCounter);
    }
}

function addBook(pointedButton, buttonImage, bookCounter){
    let csrf_token_entry = $('input[name="csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        url: 'favorite_book/',
        type: 'POST',
        dataType: 'json',
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(request) {
            buttonImage.src = yellow_star_src;
            pointedButton.setAttribute('value', "Added");
            bookCounter.innerHTML = request.book_amount;
        }
    });
}

function removeBook(pointedButton, buttonImage, bookCounter){
    let csrf_token_entry = $('input[name="csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        url: 'unfavorite_book/',
        type: 'POST',
        dataType: 'json',
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(request) {
            buttonImage.src = grey_star_src;
            pointedButton.setAttribute('value', "notAdded");
            bookCounter.innerHTML = request.book_amount;
        }
    });
}