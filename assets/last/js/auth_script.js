$(document).ready(function () {
    gapi.load('auth2', function() {
     gapi.auth2.init();
   });
});

function onSignIn(googleUser) {
    let id_token = googleUser.getAuthResponse().id_token; 
    let csrf_token_entry = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "",
        data: {
            id_token: id_token
        },
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function (result) {
            if (result.status == 0) {
                window.location.replace(result.url)
            } 
        },
        error: function (error) {
            alert("Something went error")
        }
    })
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        let csrf_token_entry = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "logout/",
            beforeSend: function(request) {
                request.setRequestHeader('X-CSRFToken', csrf_token_entry);
            },
            success: function (result) {
                window.location.replace(result.url);
            },
            error: function (error) {
                alert("Something went error")
            }
        })
    });
}
