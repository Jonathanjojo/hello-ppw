function listUser(){
    let list = document.getElementById("user-list");
    $.ajax({
        url: '/registration/user_list/',
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            list.innerHTML = '';
            result.users.forEach(user => {
                let row = document.createElement("div");
                row.classList.add(...["row", "align-items-center", "text-center"]);
                let col = document.createElement("div");
                col.classList.add(...["col", "status-entry"]);
                let text = document.createElement("h5");
                text.classList.add("light-grey-text");
                text.innerText = user.username;
                col.appendChild(text);
                row.appendChild(col);
                let unsub_button = document.createElement("button");
                unsub_button.innerText = "Unsubscribe";
                unsub_button.addEventListener("click", function() {
                    validateUnsub(user.id);
                })
                let buttondiv = document.createElement("div");
                buttondiv.classList.add(...["row", "d-flex", "justify-content-center", "text-center"]);
                buttondiv.appendChild(unsub_button);
                list.appendChild(row);
                list.appendChild(buttondiv);
            });   
        }
    });
}

function validateUnsub(user_id){
    var password = prompt("Please enter your password:", "");
    if (password != null && password != "") {
        unsub(user_id, password);
    }
}

function unsub(user_id, password_entry){
    let csrf_token_entry = $('input[name="csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        url: '/registration/user_unsub/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            id : user_id,
            password : password_entry,
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(result) {
            if(result['pswd_confirmed']){
                alert("Unsubscribed, It is sad to see you leave :(");
                listUser();
            } else {
                alert("Wrong password");
            }
        }
    });
}

function validateUserInfo() {
    event.preventDefault(true);
    let fields = document.forms["user_info_form"].getElementsByTagName("input");
    let csrf_token_entry = fields[0].value;
    let username_entry = fields[1].value;
    let email_entry = fields[2].value;
    let password_entry = fields[3].value;
    let confirmation_password_entry = fields[4].value;
    $.ajax({
        url: '/registration/user_check/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            username : username_entry,
            email : email_entry,
            password : password_entry,
            confirmation_password : confirmation_password_entry
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(result) {
            let feedback = document.getElementById("feedback");
            let button = document.getElementById("submit-button");
            feedback.classList.remove("alert-text", "blue-text");
            if(result['valid'] && result['pswd_confirmed']){
                if(result['existing_user']){
                    button.disabled = true;
                    feedback.classList.add("alert-text");
                    feedback.innerText = "There is already a User with that email";
                } else { 
                    button.disabled = false;
                    feedback.classList.add("blue-text");
                    feedback.innerText = "User info available";
                }
            } else if (!result['valid']){
                button.disabled = true;
                feedback.classList.add("alert-text");
                feedback.innerText = "Your entry is not valid";
            } else if(!result['pswd_confirmed']){
                button.disabled = true;
                feedback.classList.add("alert-text");
                feedback.innerText = "Your password confirmation does not match";    
            }
        }
    });
}

function createUser(){
    event.preventDefault(true);
    let form = document.forms["user_info_form"];
    let fields = form.getElementsByTagName("input");
    let csrf_token_entry = fields[0].value;
    let username_entry = fields[1].value;
    let email_entry = fields[2].value;
    let password_entry = fields[3].value;
    $.ajax({
        url: '/registration/user_create/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            username : username_entry,
            email : email_entry,
            password : password_entry,
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        complete: function(result) {
            let feedback = document.getElementById("feedback");
            feedback.classList.remove("alert-text", "blue-text");
            feedback.classList.add("blue-text");
            feedback.innerHTML = "Your account has been added successfully";
            let button = document.getElementById("submit-button");
            button.disabled = true;
            form.reset();
            listUser();
        }
    });
}