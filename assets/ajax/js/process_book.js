function postBook(event, bookID, bookTitle, bookPublisher){
    event.preventDefault(true);
    $.ajax({
        url: 'https://hello-joe.herokuapp.com/ajax/json_process/',
        type: 'POST',
        dataType: 'application/json',
        data: JSON.stringify({
            title : bookTitle,
            publisher : bookPublisher,
        }),
        complete: function(result) {
            toggleBookAddition(bookID);
        }
    });
}

function searchBook(){
    var searchQuery = document.getElementById("searchInput").value;
    $.ajax({
        url: 'https://hello-joe.herokuapp.com/ajax/' + searchQuery + '/',
        type: 'GET',
        dataType: 'text',
        success: function(result) {
            updateTable(result);
        }
    });
}

function updateTable(result){
    var original_table = document.getElementById('book-table');
    original_table.remove();
    var table_holder = document.getElementById('table-holder');
    table_holder.innerHTML = result;
}

function toggleBookAddition(bookID){
    var pointedButton = document.getElementById(bookID);
    var bookCounter = document.getElementById("book-counter");
    var buttonImage = document.getElementById(bookID+"-image");
    if(pointedButton.getAttribute('value') != "Added"){
        addBook(pointedButton, buttonImage, bookCounter);
    } else {
        removeBook(pointedButton, buttonImage, bookCounter);
    }
}

function addBook(pointedButton, buttonImage, bookCounter){
    buttonImage.src = yellow_star_src;
    pointedButton.setAttribute('value', "Added");
    bookCounter.innerHTML = parseInt(bookCounter.innerHTML) + 1;
}

function removeBook(pointedButton, buttonImage, bookCounter){
    buttonImage.src = grey_star_src;
    pointedButton.setAttribute('value', "notAdded");
    bookCounter.innerHTML = parseInt(bookCounter.innerHTML) - 1;
}