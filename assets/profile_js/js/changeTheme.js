function toggle_style(){
  if(document.getElementById("toggle-style-sheet").getAttribute('value') == "LIGHT"){
      document.getElementById("toggle-style-sheet").setAttribute('value', "DARK");
      document.getElementById('page-style').setAttribute(
          "href", 
          dark_theme_src
          );
  } else {
      document.getElementById("toggle-style-sheet").setAttribute('value', "LIGHT");
      document.getElementById('page-style').setAttribute(
          "href", 
          light_theme_src
      );
  }
}