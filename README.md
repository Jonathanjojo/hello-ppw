# Hello, Apa Kabar? [![build status](https://gitlab.com/jonathanjojo/hello-ppw/badges/master/build.svg)](https://gitlab.com/jonathanjojo/hello-ppw/commits/master) [![coverage report](https://gitlab.com/Jonathanjojo/hello-ppw/badges/master/coverage.svg)](https://gitlab.com/jonathanjojo/hello-ppw/commits/master)

Jonathan's PPW Lab 6 Project 

## URL

This lab projects can be accessed from [https://hello-joe.herokuapp.com](https://hello-joe.herokuapp.com)

## Authors

* **Jonathan Christopher Jakub** - [Jonathanjojo](https://gitlab.com/Jonathanjojo)

## Acknowledgments

* PPW CSUI 2018
* Lecturer : Bu Maya Retno
* Unmentioned teaching assistants