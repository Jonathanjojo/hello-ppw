from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import unittest

class NewFunctionalStatusVisitorTest(unittest.TestCase):  
    def setUp(self):  
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)
        self.page_url = 'https://hello-joe.herokuapp.com/greeting/'

    def tearDown(self):  
        self.browser.quit()

    def test_homepage_contains_correct_contents(self):
        self.browser.get(self.page_url)

        greeting_msg = self.browser.find_element_by_tag_name('h1').text  
        self.assertIn('Hello, Apa Kabar?', greeting_msg)

    def test_status_entry(self):
        self.browser.get(self.page_url)

        inputbox = self.browser.find_element_by_class_name('fields')  
        self.assertEqual(
            inputbox.get_attribute('placeholder'), "What's on your mind?"
        )
        inputbox.send_keys('coba-coba')  
        inputbox.send_keys(Keys.ENTER)  
        time.sleep(1)  
        all_entries = self.browser.find_element_by_class_name('status-entries').text
        self.assertIn('coba-coba', all_entries)

        inputbox = self.browser.find_element_by_class_name('fields')  
        self.assertEqual(
            inputbox.get_attribute('placeholder'), "What's on your mind?"
        )
        inputbox.send_keys('X'*400)  
        inputbox.send_keys(Keys.ENTER)  
        time.sleep(1)  
        all_entries = self.browser.find_element_by_class_name('status-entries').text
        self.assertIn('X'*300, all_entries)

        self.assertNotEquals(self.browser.find_element_by_link_text("Know Me More"), None)

    def test_positioning(self):
        self.browser.get(self.page_url)
        # Test section-title is in main section
        self.assertTrue(self.browser.find_elements_by_css_selector("section.main > div > h1.section-title")[0].is_displayed())
        # Test status entry is in status-entries div
        self.assertTrue(self.browser.find_elements_by_css_selector("section.main > div.status-entries > div > div.status-entry > h4")[0].is_displayed())


    def test_css_value(self):
        self.browser.get(self.page_url)        
        # Test background color
        self.assertEqual(self.browser.find_element_by_tag_name('body').value_of_css_property('background-color'), 'rgba(0, 0, 17, 1)')
        # Test wrapped status-entry
        self.assertEqual(self.browser.find_element_by_class_name('status-entry').value_of_css_property('word-wrap'), 'break-word')

class NewFunctionalProfileVisitorTest(unittest.TestCase):
    def setUp(self):  
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)
        self.page_url = 'https://hello-joe.herokuapp.com/'

    def tearDown(self):  
        self.browser.quit()

    def test_profile_page_contains_correct_contents(self):
        self.browser.get(self.page_url)

        greeting_msg = self.browser.find_element_by_tag_name('h1').text  
        self.assertIn("Hello, I'm Jojo", greeting_msg)

        self.assertNotEquals(self.browser.find_element_by_link_text("About Me"), None)