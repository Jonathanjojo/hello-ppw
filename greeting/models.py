from django.db import models

# Create your models here.
class Status(models.Model):
    text = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "statuses"
        
    def __str__(self):
        return self.text