# Generated by Django 2.0.7 on 2018-10-11 03:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('greeting', '0007_auto_20181011_0312'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
