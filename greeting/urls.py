from django.urls import path
from . import views

app_name = "greeting"

urlpatterns = [
    path('', views.index, name="homepage"),
    path('create_status/', views.create_status, name="create_status"),
    path('profile/', views.profile, name="profile")
]