from django.shortcuts import render, redirect
from .models import Status
from . import forms

def index(request):
    statuses = Status.objects.all()
    status_form = forms.CreateStatusForm()
    response = {"statuses" : statuses, "status_form" : status_form}
    return render(request, 'greeting/homepage.html', response)

def create_status(request):
    status_form = forms.CreateStatusForm(request.POST)
    if request.method == "POST" and status_form.is_valid():
        Status.objects.create(text = status_form.cleaned_data['text'])
    return redirect('greeting:homepage')

def profile(request):
    return render(request, 'greeting/profile.html')