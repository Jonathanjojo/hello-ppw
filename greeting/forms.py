from . import models
from django import forms

class CreateStatusForm(forms.Form):
    text = forms.CharField(widget=forms.TextInput(attrs={
            "class"         : "fields",
            "required"      : True,
            "placeholder"   : "What's on your mind?"
    }), max_length=300)
    
    class Meta:
        model = models.Status