from django.apps import apps
from django.http import HttpRequest
from django.test import TestCase
from django.urls import resolve
from .apps import GreetingConfig
from .forms import CreateStatusForm
from .models import Status
from .views import index, create_status, profile

class StatusModelTest(TestCase):
    def test_default_attribute(self):
        status = Status()
        self.assertEqual(status.text, '')

    def test_status_string_representation(self):
        status = Status(text="My Status")
        self.assertEqual(str(status), status.text)

    def test_status_verbose_name_plural(self):
        self.assertEqual(str(Status._meta.verbose_name_plural), "statuses")

class StatusFormTest(TestCase):
    def test_valid_input(self):
        status_form = CreateStatusForm({'text': "New Status"})
        self.assertTrue(status_form.is_valid())
        status = Status()
        status.text = status_form.cleaned_data['text']
        status.save()
        self.assertEqual(status.text, "New Status")
        self.assertEqual(Status.objects.all().count(), 1)

    def test_blank_input(self):
        form = CreateStatusForm({})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'text': ['This field is required.']
        })
        self.assertEqual(Status.objects.all().count(), 0)

    def test_invalid_input(self):
        status_form = CreateStatusForm({
            'text': 301 * "X"
        })
        self.assertFalse(status_form.is_valid())
        self.assertEqual(Status.objects.all().count(), 0)


class StatusCreationTests(TestCase):
    def test_create_status_url(self):
        response = self.client.post('/greeting/create_status/')
        self.assertEqual(response.status_code, 302)

    def test_using_create_status_url(self):
        found = resolve('/greeting/create_status/')
        self.assertEqual(found.func, create_status)

    def test_can_save_after_post_request(self):
        response = self.client.post('/greeting/create_status/', {'text': 'test'})
        self.assertEqual(Status.objects.count(), 1)
        new_status = Status.objects.first()
        self.assertEqual(new_status.text, 'test')
    
    def test_redirect_after_post_or_get_request(self):
        response = self.client.post('/greeting/create_status/', {'text': 'test'})
        self.assertEqual(response.status_code, 302)

    def test_homepage_uses_template(self):
        response = self.client.post('/greeting/')
        self.assertTemplateUsed(response, 'greeting/homepage.html')

class HomePageTests(TestCase):
    def test_homepage_accessible(self):
        response = self.client.get('/greeting/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/greeting/')
        self.assertEqual(found.func, index)
    
    def test_homepage_uses_correct_template(self):
        response = self.client.get('/greeting/')
        self.assertTemplateUsed(response, 'greeting/homepage.html')

    def test_homepage_greeting_message(self):
        response = self.client.get('/greeting/')
        self.assertContains(response, 'Hello,')
        self.assertContains(response, 'Apa Kabar?')

    def test_no_entry(self):
        response = self.client.get('/greeting/')
        self.assertContains(response, 'No status yet')

    def test_single_entry(self):
        Status.objects.create(text="1-status")
        response = self.client.get('/greeting/')
        self.assertContains(response, '1-status')
        self.assertEqual(Status.objects.all().count(), 1)

class ProfilePageTests(TestCase):
    def test_using_profile_url_accessible(self):
        response = self.client.get('/greeting/profile/')
        self.assertEqual(response.status_code, 200)

    def test_using_profile_func(self):
        found = resolve('/greeting/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_uses_correct_template(self):
        response = self.client.get('/greeting/profile/')
        self.assertTemplateUsed(response, 'greeting/profile.html')

    def test_profile_page_intro_message(self):
        response = self.client.get('/greeting/profile/')
        self.assertContains(response, 'Hello,')
        self.assertContains(response, "I'm Jojo")
