from django.test import TestCase
from django.apps import apps
from .apps import LastConfig
from .views import index, books_api
import json

# Create your tests here.
class AppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(LastConfig.name, 'last')
        self.assertEqual(apps.get_app_config('last').name, 'last')

class PageTest(TestCase):
    def test_redirect_to_login_page(self):
        response = self.client.get('/last/', follow=True)
        self.assertTemplateUsed(response, 'last/login.html')

    def test_json_returned_from_api(self):
        response = self.client.get('/last/books_api/quilting/')
        data = response.json()
        self.assertTrue("items" in data)

class LoggingTest(TestCase):
    def test_login(self):
        response = self.client.post('/last/login/')
        data = response.json()
        self.assertTrue("status" in data)

    def test_access_login_page(self):
        response = self.client.get('/last/login/')
        self.assertTemplateUsed(response, 'last/login.html')

    def test_logout(self):
        response = self.client.get('/last/logout/')
        data = response.json()
        self.assertTrue("url" in data)