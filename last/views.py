from django.http import JsonResponse
from django.urls import reverse
from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from google.oauth2 import id_token
import json
import requests
import google.auth.transport.requests as grequests

def index(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse("last:login"))
    return render(request, 'last/index.html')

def books_api(request, search_query):
    json_request = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + search_query + "/")
    original_data = json_request.json()
    book_queries = {
        'items' : [],
    }
    for book in original_data['items']:
        new_book = {
            'title'         : '',
            'publisher'     : '',
            'authors'       : []
        }
        if 'title' in book['volumeInfo']:
            new_book['title'] = book['volumeInfo']['title']
        if 'publisher' in book['volumeInfo']:
            new_book['publisher'] = book['volumeInfo']['publisher']
        if 'authors' in book['volumeInfo']:
            new_book['authors'] = [author for author in book['volumeInfo']['authors']]
        book_queries['items'].append(new_book)
    return JsonResponse(book_queries)

def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            idinfo = id_token.verify_oauth2_token(
                token, 
                grequests.Request(),
                "50540963988-c1ouu6d4gobg1t6amfp0gblflhikqrjp.apps.googleusercontent.com"
            )
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            request.session['user_id'] = idinfo['sub']
            request.session['name'] = idinfo['name']
            request.session['picture'] = idinfo['picture']
            request.session['book_amount'] = 0
            return JsonResponse({
                "status" : 0,
                "url": reverse("last:index")
            })
        except (ValueError, KeyError):
            return JsonResponse({
                "status": 1
            })
    return render(request, 'last/login.html')

def logout(request):
    request.session.flush()
    return JsonResponse({
        "url": reverse("last:login")
    })

def favorite_book(request):
    if request.method == "POST":
        request.session['book_amount'] += 1
    return JsonResponse({
        "book_amount" : request.session['book_amount'],
    })
        
def unfavorite_book(request):
    if request.method == "POST":
        request.session['book_amount'] -= 1
    return JsonResponse({
        "book_amount" : request.session['book_amount'],
    })