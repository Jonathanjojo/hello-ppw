from django.urls import path
from . import views

app_name = "last"

urlpatterns = [
    path('', views.index, name="index"),
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout"),
    path('books_api/<search_query>/', views.books_api, name='api'),
    path('favorite_book/', views.favorite_book, name="favorite"),
    path('unfavorite_book/', views.unfavorite_book, name="unfavorite"),
]